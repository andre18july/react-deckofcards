import React, { Component } from 'react';

class Card extends Component {

    render(){

        return(
            <div>
                <img 
                    alt={this.props.name} 
                    style={{ 
                        position: "absolute", 
                        top: '35%', 
                        left: '50%', 
                        transform: `rotate(${this.props.angle}deg)  translateX(-50%)` 
                    }}  
                    src={this.props.image}
                />
            </div>
        )
    }
}


export default Card;