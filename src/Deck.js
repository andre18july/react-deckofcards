import React, { Component } from 'react';
import Card from './Card';
import axios from 'axios';

class Deck extends Component {

    constructor(props){
        super(props);

        this.state = {
            deckId: "",
            remaining: 52,
            deck: [],
            disabledButton: false
        }

        this.getCard =  this.getCard.bind(this);
        this.reset = this.reset.bind(this);
    }

    async getDeck(){
        const urlDeck = "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1";
        try{
            const res = await axios.get(urlDeck);
            this.setState({ deckId: res.data.deck_id })

        }catch(error){
            console.log(error);
        }
    }

    getAngle(){
        const np = Math.floor(Math.random()*2) === 1 ? 1 : -1;
        const angle = Math.round(Math.random() * 20) * np;
        return angle;
    }

    async getCard(){
        const urlCard = `https://deckofcardsapi.com/api/deck/${this.state.deckId}/draw/?count=1`;
        try{
            
                this.setState({disabledButton: true})
            
                const res = await axios.get(urlCard);
                console.log(res);
                const card = res.data.cards[0];

                //console.log("getCard()" + res.data.remaining);
    
                if(this.state.remaining > 0){
                    //console.log("getCard() if res.data.remaining > 1: " + res.data.remaining);

                    
                    this.setState(prev => ({
                        deck: [...prev.deck, {code: card.code, value: card.value, suit: card.suit, image: card.image, angle: this.getAngle()}],
                        remaining: res.data.remaining,
                        disabledButton: false
                    }))
                }
                
        }catch(error){
            console.log(error);
        }
        
    }

    componentDidMount(){
        this.getDeck();
    }

    reset(){
        this.setState({
            deckId: "",
            remaining: 52,
            deck: [],
            disabledButton: false
        })
        this.getDeck();
    }

    render(){

        let cards = null;

        const message = <h3>There are no more cards</h3>
  
        if(this.state.deck.length > 0){
            cards = this.state.deck.map(card => {
                return <Card key={card.code} name={`${card.value} ${card.suit}`} image={card.image} angle={card.angle}/>;
            });
        }

        console.log(this.state.remaining);

 
        return(
            <div>
                <h1>Master Deck</h1>
                <h4>Cards remaining: {this.state.remaining}</h4>
                {this.state.remaining > 0 ? 
                    <button onClick={this.getCard} disabled={this.state.disabledButton}>Get Card</button> :
                    <button onClick={this.reset}>Reset</button> }
                {this.state.remaining > 0 ? cards : message}
            </div>

        )
    }
}

export default Deck;